#include <iostream>
#include <iomanip>
#include "GameEngine.h"

int main() {
    std::string startGame;
    std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
    std::cout << std::setw(65) << "Welcome to the space building simulator." << std::endl;
    std::cout << std::setw(65) << "You are captain of your own spaceship." << std::endl;
    std::cout << std::setw(85) << "Your goal is to conquer all Planets and build advanced civilization on them." << std::endl;
    std::cout << "In this game you can conquer Planets, upgrade your Spaceship, build Buildings and upgrade them." << std::endl;
    std::cout <<"-----------------------------------------------------------------------------------------------" << std::endl;
    std::cout << std::setw(60) << "Press any button to continue." << std::endl;
    std::cout <<"-----------------------------------------------------------------------------------------------" << std::endl;
    std::cin >> startGame;
    if (startGame != ""){
        GameEngine* game = new GameEngine();
        game->playGame();
        delete game;
    }

    return 0;
}
