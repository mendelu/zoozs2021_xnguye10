//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_STORAGE_H
#define OOPPROJECT_STORAGE_H


class Storage {
    int m_maxSilicon;
    int m_maxWolfram;
    int m_maxTitanium;
    int m_currSilicon;
    int m_currWolfram;
    int m_currTitanium;
public:
    Storage(int maxSilicon, int maxWolfram, int maxTitanium);
    void printInfo();
    void addSilicon(int silicon);
    void addWolfram(int wolfram);
    void addTitanium(int titanium);
    void decreaseSilicon(int silicon);
    void decreaseWolfram(int wolfram);
    void decreaseTitanium(int titanium);
    int getSilicon();
    int getWolfram();
    int getTitanium();
    int getMaxSilicon();
    int getMaxWolfram();
    int getMaxTitanium();
};


#endif //OOPPROJECT_STORAGE_H
