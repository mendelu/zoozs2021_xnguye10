//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_BUILDING_H
#define OOPPROJECT_BUILDING_H


#include <string>

class Building {
    std::string m_type;
    int m_prodPerTurn;
    int m_publicOrderPerTurn;
    int m_prodMultiplier;
    int m_energyConsumption;
    int m_energyProduction;
    int m_lvl;
public:
    Building();
    void printInfo();
    void setType(std::string type);
    void setProdPerTurn(int prodPerTurn);
    void setLvl(int lvl);
    void setPublicOrderPerTurn(int publicOrderPerTurn);
    void setProdMultiplier(int prodMultiplier);
    void setEnergyConsumption(int energyConsumption);
    void setEnergyProduction(int energyProduction);
    std::string getType();
    int getPublicOrderPerTurn();
    int getLvl();
    int getProdPerTurn();
    void lvlCost(std::string costII, std::string costIII);
};


#endif //OOPPROJECT_BUILDING_H
