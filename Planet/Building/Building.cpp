//
// Created by ruzar on 30.11.2021.
//

#include "Building.h"
#include <iostream>

Building::Building() {
    m_lvl = 1;
    m_prodPerTurn = 0;
    m_publicOrderPerTurn = 0;
    m_prodMultiplier = 0;
    m_energyConsumption = 0;
    m_energyProduction = 0;
}

void Building::printInfo() {
    std::cout << " " << m_type << " --" << std::endl;
    if(m_prodPerTurn != 0){
        std::cout << "Production per turn: " << m_prodPerTurn << std::endl;
    }
    if(m_publicOrderPerTurn != 0){
        if(m_type == "Power Plant" || m_type == "Silicon Mine" || m_type == "Wolfram Mine"|| m_type == "Titanium Mine") {
            std::cout << "Public order per turn: -" << m_publicOrderPerTurn << std::endl;
        }else{
            std::cout << "Public order per turn: " << m_publicOrderPerTurn << std::endl;
        }
    }
    if(m_prodMultiplier != 0){
        std::cout << "Bonus to production: " << m_prodMultiplier << std::endl;
    }
    if(m_energyConsumption != 0){
        std::cout << "Energy consumption: " << m_energyConsumption << std::endl;
    }
    if(m_energyProduction != 0){
        std::cout << "Energy production: " << m_energyProduction << std::endl;
    }

    std::cout << "-------------------------" << std::endl;
}

void Building::setType(std::string type) {
    m_type =type;
}

void Building::setProdPerTurn(int prodPerTurn) {
    m_prodPerTurn = prodPerTurn;
}

void Building::setLvl(int lvl) {
    m_lvl = lvl;
}

void Building::setPublicOrderPerTurn(int publicOrderPerTurn) {
    m_publicOrderPerTurn = publicOrderPerTurn;
}

void Building::setProdMultiplier(int prodMultiplier) {
    m_prodMultiplier = prodMultiplier;
}

void Building::setEnergyConsumption(int energyConsumption) {
    m_energyConsumption = energyConsumption;
}

void Building::setEnergyProduction(int energyProduction) {
    m_energyProduction = energyProduction;
}

std::string Building::getType() {
    return m_type;
}

int Building::getPublicOrderPerTurn() {
    return m_publicOrderPerTurn;
}

int Building::getLvl() {
    return m_lvl;
}

int Building::getProdPerTurn() {
    return m_prodPerTurn;
}

void Building::lvlCost(std::string costII, std::string costIII) {
    std::cout << "Lvl II -> "<< costII << std::endl;
    std::cout << "Lvl III -> " << costIII << std::endl;
}
