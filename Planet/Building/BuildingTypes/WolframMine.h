//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_WOLFRAMMINE_H
#define OOPPROJECT_WOLFRAMMINE_H


#include "../BuildingDirector/Builder.h"

class WolframMine: public Builder {
    void setAttributes() override;
};


#endif //OOPPROJECT_WOLFRAMMINE_H
