//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_POWERPLANT_H
#define OOPPROJECT_POWERPLANT_H

#include "../BuildingDirector/Builder.h"

class PowerPlant: public Builder {
public:
    void setAttributes() override;
};


#endif //OOPPROJECT_POWERPLANT_H
