//
// Created by ruzar on 30.11.2021.
//

#include <iostream>
#include "Bar.h"

void Bar::setAttributes() {
    m_building->setType("Bar");
    m_building->setProdPerTurn(0);
    m_building->setPublicOrderPerTurn(20);
    m_building->setProdMultiplier(0);
    m_building->setEnergyConsumption(10);
    m_building->setEnergyProduction(0);
    m_building->lvlCost("100 Titanium", "100 Silicon");
}
