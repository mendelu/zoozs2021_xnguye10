//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_SILICONMINE_H
#define OOPPROJECT_SILICONMINE_H


#include "../BuildingDirector/Builder.h"

class SiliconMine: public Builder {
    void setAttributes() override;
};


#endif //OOPPROJECT_SILICONMINE_H
