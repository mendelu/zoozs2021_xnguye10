//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_TITANIUMMINE_H
#define OOPPROJECT_TITANIUMMINE_H


#include "../BuildingDirector/Builder.h"

class TitaniumMine: public Builder {
    void setAttributes() override;
};


#endif //OOPPROJECT_TITANIUMMINE_H
