//
// Created by ruzar on 30.11.2021.
//

#include <iostream>
#include "TitaniumMine.h"

void TitaniumMine::setAttributes() {
    m_building->setType("Titanium Mine");
    m_building->setProdPerTurn(10);
    m_building->setPublicOrderPerTurn(10);
    m_building->setProdMultiplier(0);
    m_building->setEnergyConsumption(20);
    m_building->setEnergyProduction(0);
    m_building->lvlCost("50 Wolfram", "150 Titanium");
}
