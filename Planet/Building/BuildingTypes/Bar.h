//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_BAR_H
#define OOPPROJECT_BAR_H


#include "../BuildingDirector/Builder.h"

class Bar: public Builder{
    void setAttributes() override;
};


#endif //OOPPROJECT_BAR_H
