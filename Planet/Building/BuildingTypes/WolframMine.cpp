//
// Created by ruzar on 30.11.2021.
//

#include <iostream>
#include "WolframMine.h"

void WolframMine::setAttributes() {
    m_building->setType("Wolfram Mine");
    m_building->setProdPerTurn(10);
    m_building->setPublicOrderPerTurn(10);
    m_building->setProdMultiplier(0);
    m_building->setEnergyConsumption(20);
    m_building->setEnergyProduction(0);
    m_building->lvlCost("50 Wolfram", "100 Silicon");
}
