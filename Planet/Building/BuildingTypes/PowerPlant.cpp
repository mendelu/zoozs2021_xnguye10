//
// Created by ruzar on 30.11.2021.
//

#include <iostream>
#include "PowerPlant.h"

void PowerPlant::setAttributes() {
    m_building->setType("Power Plant");
    m_building->setProdPerTurn(0);
    m_building->setPublicOrderPerTurn(5);
    m_building->setProdMultiplier(0);
    m_building->setEnergyConsumption(0);
    m_building->setEnergyProduction(50);
    m_building->lvlCost("100 Silicon", "200 Wolfram");
}
