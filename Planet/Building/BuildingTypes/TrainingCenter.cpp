//
// Created by ruzar on 30.11.2021.
//

#include <iostream>
#include "TrainingCenter.h"

void TrainingCenter::setAttributes() {
    m_building->setType("Training Center");
    m_building->setProdPerTurn(0);
    m_building->setPublicOrderPerTurn(5);
    m_building->setProdMultiplier(2);
    m_building->setEnergyConsumption(10);
    m_building->setEnergyProduction(0);
    m_building->lvlCost("50 Titanium", "100 Silicon");
}
