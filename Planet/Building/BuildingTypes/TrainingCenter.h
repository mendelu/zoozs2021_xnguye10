//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_TRAININGCENTER_H
#define OOPPROJECT_TRAININGCENTER_H


#include "../BuildingDirector/Builder.h"

class TrainingCenter: public Builder{
public:
    void setAttributes() override;
};


#endif //OOPPROJECT_TRAININGCENTER_H
