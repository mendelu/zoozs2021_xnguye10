//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_BUILDINGDIRECTOR_H
#define OOPPROJECT_BUILDINGDIRECTOR_H


#include "Builder.h"

class BuildingDirector {
    Builder* m_builder;
public:
    BuildingDirector();
    BuildingDirector(Builder* builder);
    void setBuilder(Builder* builder);
    Building* constructBuilding();
};


#endif //OOPPROJECT_BUILDINGDIRECTOR_H
