//
// Created by ruzar on 30.11.2021.
//

#include "Builder.h"

Builder::Builder() {
    m_building = nullptr;
}

void Builder::createBuilding() {
    m_building = new Building();
}

Building *Builder::getBuilding() {
    return m_building;
}
