//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_BUILDER_H
#define OOPPROJECT_BUILDER_H


#include "../Building.h"

class Builder {
protected:
    Building* m_building;
public:
    Builder();
    void createBuilding();
    virtual void setAttributes() = 0;
    Building* getBuilding();
};


#endif //OOPPROJECT_BUILDER_H
