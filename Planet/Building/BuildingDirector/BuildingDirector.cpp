//
// Created by ruzar on 30.11.2021.
//

#include "BuildingDirector.h"

BuildingDirector::BuildingDirector(Builder *builder) {
    m_builder = builder;
}

void BuildingDirector::setBuilder(Builder *builder) {
    m_builder = builder;
}

Building *BuildingDirector::constructBuilding() {
    m_builder->createBuilding();
    m_builder->setAttributes();
    return m_builder->getBuilding();
}

BuildingDirector::BuildingDirector() {
    m_builder = nullptr;
}
