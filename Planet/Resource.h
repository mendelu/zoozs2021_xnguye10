//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_RESOURCE_H
#define OOPPROJECT_RESOURCE_H


#include <string>

class Resource {
    std::string m_name;
    int m_amount;
    int m_minedPerTurn;
public:
    Resource(std::string name, int amount);
    std::string getName();
    int getAmount();
    void setAmount(int amount);
    int getMinedPerTurn();
};


#endif //OOPPROJECT_RESOURCE_H
