//
// Created by ruzar on 30.11.2021.
//

#include "Resource.h"

Resource::Resource(std::string name, int amount) {
    m_name = name;
    m_amount = amount;
    m_minedPerTurn = 0;
}

std::string Resource::getName() {
    return m_name;
}

int Resource::getAmount() {
    return m_amount;
}

int Resource::getMinedPerTurn() {
    return m_minedPerTurn;
}

void Resource::setAmount(int amount) {
    m_amount-= amount;
}


