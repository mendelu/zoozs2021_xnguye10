//
// Created by ruzar on 30.11.2021.
//

#ifndef OOPPROJECT_PLANET_H
#define OOPPROJECT_PLANET_H

#include <iostream>
#include <sys/time.h>
#include <string>
#include <array>
#include <vector>
#include "Building/Building.h"
#include "Resource.h"
#include "Storage.h"
#include "Building/BuildingDirector/BuildingDirector.h"
#include "Building/BuildingTypes/PowerPlant.h"
#include "Building/BuildingTypes/Bar.h"
#include "Building/BuildingTypes/TrainingCenter.h"
#include "Building/BuildingTypes/SiliconMine.h"
#include "Building/BuildingTypes/TitaniumMine.h"
#include "Building/BuildingTypes/WolframMine.h"

class Planet {
    std::string m_name;
    int m_productionMultiplier;
    int m_energy;
    int m_publicOrder;
    std::array<Resource*, 2> m_resources;
    std::array<Building*, 10> m_buildings;
    Storage* m_storage;
    int m_hullDamage;
    int m_shieldDamage;
    bool m_conquered;
    int m_planetDefenceHp;
    int m_maxPlanetDefenceHp;
    int m_planetShieldHp;
    int m_maxPlanetShieldHp;
public:
    Planet(std::string name, int hullDamage,
           int shieldDamage, int planetDefenceHp, int planetShieldHp);
    ~Planet();
    void printInfoBasic();
    void printInfoCombat();
    void printInfoBuildings();
    void printInfoProduction();
    void build();
    void upgradeBuilding(int slot);
    void updatePublicOrder();
    int getPublicOrderChangePerTurn();
    int getPublicOrder();
    void updateStorage();
    void resourceSearch();
    void initResource(int slot);
    void setConqueredToTrue();
    bool getConquered();
    int getHullDamage();
    int getShieldDamage();
    int getPlanetDefenceHp();
    int getPlanetShieldHp();
    void setPlanetDefenceHp(int dmg);
    void setPlanetDefenceHpToMax();
    void setPlanetShieldHp(int dmg);
    void setPlanetShieldHpToMax();
    void printHpAndShields();
    std::string getName();
    int getSilicon();
    int getWolfram();
    int getTitanium();
    void decreaseSilicon(int silicon);
    void decreaseWolfram(int wolfram);
    void decreaseTitanium(int titanium);
    void addSilicon(int silicon);
    void addWolfram(int wolfram);
    void addTitanium(int titanium);
    int getEmptyStorageSilicon();
    int getEmptyStorageWolfram();
    int getEmptyStorageTitanium();
};


#endif //OOPPROJECT_PLANET_H
