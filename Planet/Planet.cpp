//
// Created by ruzar on 30.11.2021.
//

#include "Planet.h"

Planet::Planet(std::string name, int hullDamage, int shieldDamage, int planetDefenceHp, int planetShieldHp){
    m_name = name;

    m_hullDamage = hullDamage;
    m_shieldDamage = shieldDamage;
    m_planetDefenceHp = planetDefenceHp;
    m_maxPlanetDefenceHp = planetDefenceHp;
    m_planetShieldHp = planetShieldHp;
    m_maxPlanetShieldHp = planetShieldHp;
    m_conquered = 0;
    m_energy = 0;
    m_publicOrder = 0;
    m_productionMultiplier = 1;
    m_resources[1] = nullptr;
    m_storage = new Storage(1000,1000,1000);
    for (int i = 0; i < m_buildings.size(); ++i) {
        m_buildings.at(i) = nullptr;
    }
    initResource(0);
}

Planet::~Planet() {
 delete m_storage;
}

std::string Planet::getName() {
    return m_name;
}

int Planet::getHullDamage(){
    return m_hullDamage;
}

int Planet::getShieldDamage(){
    return m_shieldDamage;
}

int Planet::getPlanetDefenceHp(){
    return m_planetDefenceHp;
}

int Planet::getPlanetShieldHp(){
    return m_planetShieldHp;
}

void Planet::setPlanetDefenceHp(int dmg){
    if((m_planetDefenceHp - dmg) < 0){
        m_planetDefenceHp = 0;
    } else {
        m_planetDefenceHp = m_planetDefenceHp - dmg;
    }
}

void Planet::setPlanetDefenceHpToMax() {
    m_planetDefenceHp = m_maxPlanetDefenceHp;
}

void Planet::setPlanetShieldHpToMax() {
    m_planetShieldHp = m_maxPlanetShieldHp;
}

void Planet::setPlanetShieldHp(int dmg){
    if((m_planetShieldHp - dmg) < 0){
        m_planetShieldHp = 0;
    } else {
        m_planetShieldHp = m_planetShieldHp - dmg;
    }
}

void Planet::printInfoBasic() {
    std::cout << "-- " << m_name << " --" << std::endl;
    std::cout << "Public order:  " << m_publicOrder << "(" << getPublicOrderChangePerTurn() << ")" << std::endl;
    if(m_conquered){
        std::cout << "Conquered: Yes" << std::endl;
    }else{
        std::cout << "Conquered: No" << std::endl;
    }
    std::cout << "-------------" << std::endl;
}

void Planet::printInfoCombat() {
    std::cout << "-- Planet combat Info --" << std::endl;
    std::cout << "Planetary defence HP: " << m_planetDefenceHp << std::endl;
    std::cout << "Planetary shield HP: " << m_planetShieldHp << std::endl;
    std::cout << "Hull damage: " << m_hullDamage << std::endl;
    std::cout << "Shield damage: " << m_shieldDamage << std::endl;
    std::cout << "-------------" << std::endl;
    std::cout << std::endl;
}

void Planet::printInfoBuildings() {
    std::cout << "--- BUILDINGS ---" << std::endl;
    for (int i = 0; i < m_buildings.size(); ++i) {
        if (m_buildings.at(i) != nullptr){
            std::cout << "-- [" << i+1 << "]";
            m_buildings[i]->printInfo();
        }
    }
    std::cout << "-------------" << std::endl;
}

void Planet::printInfoProduction() {
    std::cout << "-- Production Info --" << std::endl;
    std::cout << "Available resources: ";
    for (int i = 0; i < m_resources.size(); ++i) {
        if (m_resources.at(i) != nullptr){
            std::cout << m_resources[i]->getName() << ": " << m_resources[i]->getAmount() << ", ";
        }else{
            std::cout << "-";
        }
    }
    std::cout << std::endl;
    std::cout << "Energy production: " << m_energy << std::endl;
    std::cout << "Production multiplier: " << m_productionMultiplier << std::endl;
    m_storage->printInfo();
}

void Planet::build() {
    BuildingDirector* buildingDirector = new BuildingDirector();
    int type;
    int slot;
    std::cout << "[1] - Power plant - 100 Wolfram" << std::endl;
    std::cout << "[2] - Training center - 100 Silicon, 100 Wolfram" << std::endl;
    std::cout << "[3] - Bar - 50 Silicon, 50 Titanium" << std::endl;
    std::cout << "[4]" << " - " << m_resources[0]->getName() << " mine - 100 Titanium"<< std::endl;
    if (m_resources[1] != nullptr){
        std::cout << "[5]" << " - " << m_resources[1]->getName() << " mine - 100 Titanium"<< std::endl;
    }
    std::cin >> type;
    if (type<1 ||type>5){
            std::cout << "Not a valid answer" << std::endl;
    }else {
        std::cout << "Choose a slot, 1-10" << std::endl;
        std::cin >> slot;
        if (m_buildings.at(slot) != nullptr) {
            std::cout << "Another building is already built here." << std::endl;
        } else {
            switch (type) {
                case 1: {
                    if (m_storage->getWolfram() - 100 < 0) {
                        std::cout << "Not enough resources" << std::endl;
                    } else {
                        m_storage->decreaseWolfram(100);
                        buildingDirector->setBuilder(new PowerPlant());
                        Building *powerPlant = buildingDirector->constructBuilding();
                        m_buildings.at(slot - 1) = powerPlant;
                        m_energy += 50;
                    }
                    break;
                }
                case 2: {
                    if ((m_energy - 10) < 0) {
                        std::cout
                                << "Energy production of this planet cant sustain this building. Build another power plant first."
                                << std::endl;
                    } else {
                        if (m_storage->getWolfram() - 100 < 0 || m_storage->getSilicon() - 100 < 0) {
                            std::cout << "Not enough resources" << std::endl;
                        } else {
                            m_storage->decreaseWolfram(100);
                            m_storage->decreaseSilicon(100);
                            buildingDirector->setBuilder(new TrainingCenter());
                            Building *trainingCenter = buildingDirector->constructBuilding();
                            m_buildings.at(slot - 1) = trainingCenter;
                            m_energy -= 10;
                            m_productionMultiplier += 2;
                        }
                    }
                    break;
                }
                case 3: {
                    if ((m_energy - 10) < 0) {
                        std::cout
                                << "Energy production of this planet cant sustain this building. Build another power plant first."
                                << std::endl;
                    } else {
                        if (m_storage->getTitanium() - 50 < 0 || m_storage->getSilicon() - 50 < 0) {
                            std::cout << "Not enough resources" << std::endl;
                        } else {
                            m_storage->decreaseTitanium(50);
                            m_storage->decreaseSilicon(50);
                            buildingDirector->setBuilder(new Bar());
                            Building *bar = buildingDirector->constructBuilding();
                            m_buildings.at(slot - 1) = bar;
                            m_energy -= 10;
                        }
                    }
                    break;
                }
                case 4: {
                    if ((m_energy - 20) < 0) {
                        std::cout
                                << "Energy production of this planet cant sustain this building. Build another power plant first."
                                << std::endl;
                    } else {
                        if (m_storage->getTitanium() - 100 < 0) {
                            std::cout << "Not enough resources" << std::endl;
                        } else {
                            m_storage->decreaseTitanium(100);
                            if (m_resources[0]->getName() == "Titanium") {
                                buildingDirector->setBuilder(new TitaniumMine());
                                Building *titaniumMine = buildingDirector->constructBuilding();
                                m_buildings.at(slot - 1) = titaniumMine;
                            } else if (m_resources[0]->getName() == "Silicon") {
                                buildingDirector->setBuilder(new SiliconMine());
                                Building *siliconMine = buildingDirector->constructBuilding();
                                m_buildings.at(slot - 1) = siliconMine;
                            } else if (m_resources[0]->getName() == "Wolfram") {
                                buildingDirector->setBuilder(new WolframMine());
                                Building *wolframMine = buildingDirector->constructBuilding();
                                m_buildings.at(slot - 1) = wolframMine;
                            }
                            m_energy -= 20;
                        }
                    }
                    break;
                }
                case 5: {
                    if ((m_energy - 20) < 0) {
                        std::cout
                                << "Energy production of this planet cant sustain this building. Build another power plant first."
                                << std::endl;
                    } else {
                        if (m_storage->getTitanium() - 100 < 0) {
                            std::cout << "Not enough resources" << std::endl;
                        } else {
                            m_storage->decreaseTitanium(100);
                            if (m_resources[1]->getName() == "Titanium") {
                                buildingDirector->setBuilder(new TitaniumMine());
                                Building *titaniumMine = buildingDirector->constructBuilding();
                                m_buildings.at(slot - 1) = titaniumMine;
                            } else if (m_resources[1]->getName() == "Silicon") {
                                buildingDirector->setBuilder(new SiliconMine());
                                Building *siliconMine = buildingDirector->constructBuilding();
                                m_buildings.at(slot - 1) = siliconMine;
                            } else if (m_resources[1]->getName() == "Wolfram") {
                                buildingDirector->setBuilder(new WolframMine());
                                Building *wolframMine = buildingDirector->constructBuilding();
                                m_buildings.at(slot - 1) = wolframMine;
                            }
                            m_energy -= 20;
                        }
                    }
                    break;
                }
            }
        }
    }
}

void Planet::upgradeBuilding(int slot) {
    Building* building = m_buildings.at(slot-1);
    if(building->getLvl()==1){
        if(building->getType()=="Bar"){
            if (m_storage->getTitanium() - 100 < 0){
                std::cout << "Not enough resources" << std::endl;
            }else {
                m_storage->decreaseTitanium(100);
                building->setPublicOrderPerTurn(25);
                building->setEnergyConsumption(10);
                building->setLvl(2);
            }
        }else if(m_buildings.at(slot-1)->getType()=="Training center"){
            if (m_storage->getTitanium() - 50 < 0){
                std::cout << "Not enough resources" << std::endl;
            }else {
                m_storage->decreaseTitanium(50);
                building->setPublicOrderPerTurn(5);
                building->setProdMultiplier(3);
                building->setEnergyConsumption(10);
                building->setLvl(2);
            }
        }else if(m_buildings.at(slot-1)->getType()=="Power Plant"){
            if (m_storage->getSilicon() - 100 < 0){
                std::cout << "Not enough resources" << std::endl;
            }else {
                m_storage->decreaseSilicon(100);
                building->setPublicOrderPerTurn(5);
                building->setEnergyProduction(60);
                building->setLvl(2);
            }
        }else{
            if (m_storage->getWolfram() - 50 < 0){
                std::cout << "Not enough resources" << std::endl;
            }else {
                m_storage->decreaseWolfram(50);
                building->setProdPerTurn(20);
                building->setPublicOrderPerTurn(15);
                building->setEnergyConsumption(25);
                building->setLvl(2);
            }
        }
    }else if(m_buildings.at(slot-1)->getLvl()==2){
        if(building->getType()=="Bar"){
            if (m_storage->getSilicon() - 100 < 0){
                std::cout << "Not enough resources" << std::endl;
            }else {
                m_storage->decreaseSilicon(100);
                building->setPublicOrderPerTurn(30);
                building->setEnergyConsumption(15);
                building->setLvl(3);
            }
        }else if(m_buildings.at(slot-1)->getType()=="Training center"){
            if (m_storage->getSilicon() - 100 < 0){
                std::cout << "Not enough resources" << std::endl;
            }else {
                m_storage->decreaseSilicon(100);
                building->setPublicOrderPerTurn(10);
                building->setProdMultiplier(4);
                building->setEnergyConsumption(15);
                building->setLvl(3);
            }
        }else if(m_buildings.at(slot-1)->getType()=="Power Plant"){
            if (m_storage->getWolfram() - 200 < 0){
                std::cout << "Not enough resources" << std::endl;
            }else {
                m_storage->decreaseWolfram(200);
                building->setPublicOrderPerTurn(5);
                building->setEnergyProduction(100);
                building->setLvl(3);
            }
        }else{
            if (m_storage->getTitanium() - 150 < 0){
                std::cout << "Not enough resources" << std::endl;
            }else {
                m_storage->decreaseTitanium(150);
                building->setProdPerTurn(30);
                building->setPublicOrderPerTurn(15);
                building->setEnergyConsumption(30);
                building->setLvl(3);
            }
        }
    }else{
        std::cout << "Building is already at max level." << std::endl;
    }
}

void Planet::resourceSearch() {
    int check;
    if (m_resources[1] == nullptr){
        std::cout << "Search " << m_name << " for additional resources? -- Cost: 150 Silicon, 100 Titanium, Yes[1]/No[0]" << std::endl;
        std::cin >> check;
        if (check == 1){
            if (m_storage->getSilicon() - 150 < 0 || m_storage->getTitanium() - 100 < 0){
                std::cout << "Not enough resources" << std::endl;
            }else{
                m_storage->decreaseSilicon(150);
                m_storage->decreaseTitanium(100);
                initResource(1);
                while (m_resources[0]->getName() == m_resources[1]->getName()){
                    initResource(1);
                }
            }
        }
    }else{
        std::cout << "All resources already found" << std::endl;
    }
}

void Planet::initResource(int slot) {
    int seed = m_name.at(1) + m_name.at(2) + m_name.at(3);
    srand(seed + (int) time(NULL));
    int num = rand() % 2;
    switch (num) {
        case 0: {
            Resource* silicon = new Resource("Silicon", 10 * (rand() % 70 + 30));
            m_resources[slot] = silicon;
            break;
        }
        case 1: {
            Resource* wolfram = new Resource("Wolfram", 10 * (rand() % 70 + 30));
            m_resources[slot] = wolfram;
            break;
        }
        case 2: {
            Resource* titanium = new Resource("Titanium", 10 * (rand() % 70 + 30));
            m_resources[slot] = titanium;
            break;
        }
    }
}

int Planet::getPublicOrderChangePerTurn() {
    int changePerTurn = 0;
    for (int i = 0; i < m_buildings.size(); ++i) {
        if (m_buildings.at(i)!= nullptr){
            if (m_buildings.at(i)->getType()=="Bar" || m_buildings.at(i)->getType()=="Training Center"){
                changePerTurn+=m_buildings.at(i)->getPublicOrderPerTurn();
            }else{
                changePerTurn-=m_buildings.at(i)->getPublicOrderPerTurn();
            }
        }
    }

    return changePerTurn;
}

void Planet::updatePublicOrder() {
    if(m_publicOrder>-100 || m_publicOrder<100) {
        m_publicOrder += getPublicOrderChangePerTurn();
    }
    if(m_publicOrder<=-100){
        m_conquered = false;
    }
}

void Planet::updateStorage() {
    for (int i = 0; i < m_buildings.size(); ++i) {
        if (m_buildings.at(i)!= nullptr){
            if (m_buildings.at(i)->getType()=="Titanium Mine"){
                m_storage->addTitanium(m_productionMultiplier*(m_buildings.at(i)->getProdPerTurn()));
                if(m_resources[0]->getName()=="Titanium"){
                    m_resources[0]->setAmount(m_productionMultiplier*(m_buildings.at(i)->getProdPerTurn()));
                }else{
                    m_resources[1]->setAmount(m_productionMultiplier*(m_buildings.at(i)->getProdPerTurn()));
                }
            }else if(m_buildings.at(i)->getType()=="Wolfram Mine"){
                m_storage->addWolfram(m_productionMultiplier*(m_buildings.at(i)->getProdPerTurn()));
                if(m_resources[0]->getName()=="Wolfram"){
                    m_resources[0]->setAmount(m_productionMultiplier*(m_buildings.at(i)->getProdPerTurn()));
                }else{
                    m_resources[1]->setAmount(m_productionMultiplier*(m_buildings.at(i)->getProdPerTurn()));
                }
            }else if(m_buildings.at(i)->getType()=="Silicon Mine"){
                m_storage->addSilicon(m_productionMultiplier*(m_buildings.at(i)->getProdPerTurn()));
                if(m_resources[0]->getName()=="Silicon"){
                    m_resources[0]->setAmount(m_productionMultiplier*(m_buildings.at(i)->getProdPerTurn()));
                }else{
                    m_resources[1]->setAmount(m_productionMultiplier*(m_buildings.at(i)->getProdPerTurn()));
                }
            }
        }
    }
}

void Planet::setConqueredToTrue(){
    m_conquered = true;
}

bool Planet::getConquered() {
    return m_conquered;
}

int Planet::getPublicOrder() {
    return m_publicOrder;
}

void Planet::printHpAndShields(){
    std::cout << " --- Planet --- " << std::endl;
    std::cout << " Planet HP = " << m_planetDefenceHp << std::endl;
    std::cout << " Planet Shields = " << m_planetShieldHp << std::endl;
}

int Planet::getSilicon() {
    return m_storage->getSilicon();
}

int Planet::getWolfram() {
    return m_storage->getWolfram();
}

int Planet::getTitanium() {
    return m_storage->getTitanium();
}

void Planet::decreaseSilicon(int silicon) {
    m_storage->decreaseSilicon(silicon);
}

void Planet::decreaseWolfram(int wolfram) {
    m_storage->decreaseWolfram(wolfram);
}

void Planet::decreaseTitanium(int titanium) {
    m_storage->decreaseTitanium(titanium);
}

void Planet::addSilicon(int silicon) {
    m_storage->addSilicon(silicon);
}

void Planet::addWolfram(int wolfram) {
    m_storage->addWolfram(wolfram);
}

void Planet::addTitanium(int titanium) {
    m_storage->addTitanium(titanium);
}

int Planet::getEmptyStorageSilicon() {
    return m_storage->getMaxSilicon() - m_storage->getSilicon();
}

int Planet::getEmptyStorageWolfram() {
    return m_storage->getMaxWolfram() - m_storage->getWolfram();
}

int Planet::getEmptyStorageTitanium() {
    return m_storage->getMaxTitanium() - m_storage->getTitanium();
}
