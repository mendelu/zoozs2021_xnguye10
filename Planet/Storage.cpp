//
// Created by ruzar on 30.11.2021.
//

#include <iostream>
#include "Storage.h"

Storage::Storage(int maxSilicon, int maxWolfram, int maxTitanium) {
    m_maxSilicon = maxSilicon;
    m_maxWolfram = maxWolfram;
    m_maxTitanium = maxTitanium;
    m_currSilicon = 0;
    m_currWolfram = 0;
    m_currTitanium = 0;
}

void Storage::printInfo() {
    std::cout << "-- Storage --" << std::endl;
    std::cout << "Silicon: " << m_currSilicon << "/" << m_maxSilicon << std::endl;
    std::cout << "Wolfram: " << m_currWolfram << "/" << m_maxWolfram << std::endl;
    std::cout << "Titanium: " << m_currTitanium << "/" << m_maxTitanium << std::endl;
    std::cout << "-------------" << std::endl;
}

void Storage::addSilicon(int silicon) {
    m_currSilicon += silicon;
}

void Storage::addWolfram(int wolfram) {
    m_currWolfram += wolfram;
}

void Storage::addTitanium(int titanium) {
    m_currTitanium += titanium;
}

void Storage::decreaseSilicon(int silicon) {
    m_currSilicon -= silicon;
}

void Storage::decreaseWolfram(int wolfram) {
    m_currWolfram -= wolfram;
}

void Storage::decreaseTitanium(int titanium) {
    m_currTitanium -= titanium;
}

int Storage::getSilicon() {
    return m_currSilicon;
}

int Storage::getWolfram() {
    return m_currWolfram;
}

int Storage::getTitanium() {
    return m_currTitanium;
}

int Storage::getMaxSilicon() {
    return m_maxSilicon;
}

int Storage::getMaxWolfram() {
    return m_maxWolfram;
}

int Storage::getMaxTitanium() {
    return m_maxTitanium;
}
