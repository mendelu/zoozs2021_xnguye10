//
// Created by nuguy on 09.12.2021.
//

#include <iostream>
#include "WeaponSystems.h"

WeaponSystems::WeaponSystems(std::string name, int damagePerTurn) {
    m_name = name;
    m_damagePerTurn = damagePerTurn;
    m_lvl = 1;
}

int WeaponSystems::getDamagePerTurn() {
    return m_damagePerTurn;
}

std::string WeaponSystems::getName() {
    return m_name;
}

int WeaponSystems::getLvl() {
    return m_lvl;
}

void WeaponSystems::upgradeWeapon(int slot) {
    m_lvl++;
    m_damagePerTurn += m_damagePerTurn*0.2;
}


