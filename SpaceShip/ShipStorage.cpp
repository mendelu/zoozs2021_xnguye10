//
// Created by nuguy on 09.12.2021.
//

#include "ShipStorage.h"

ShipStorage::ShipStorage(int maxSilicon, int maxWolfram, int maxTitanium) {
    m_lvl = 1;
    m_maxSilicon = maxSilicon;
    m_maxWolfram = maxWolfram;
    m_maxTitanium = maxTitanium;
    m_currSilicon = 100;
    m_currWolfram = 100;
    m_currTitanium = 100;
}

void ShipStorage::lvlUp() {
    m_lvl++;
}

int ShipStorage::getLvl() {
    return m_lvl;
}

int ShipStorage::getCurrSilicon() {
    return m_currSilicon;
}

int ShipStorage::getCurrWolfram() {
    return m_currWolfram;
}

int ShipStorage::getCurrTitanium() {
    return m_currTitanium;
}

int ShipStorage::getMaxSilicon() {
    return m_maxSilicon;
}

int ShipStorage::getMaxWolfram() {
    return m_maxWolfram;
}

int ShipStorage::getMaxTitanium() {
    return m_maxTitanium;
}

int ShipStorage::addSilicon(int silicon) {
    m_currSilicon += silicon;
}

int ShipStorage::addWolfram(int wolfram) {
    m_currWolfram += wolfram;
}

int ShipStorage::addTitanium(int titanium) {
    m_currTitanium += titanium;
}

int ShipStorage::decreaseSilicon(int silicon) {
    m_currSilicon -= silicon;
}

int ShipStorage::decreaseWolfram(int wolfram) {
    m_currWolfram -= wolfram;
}

int ShipStorage::decreaseTitanium(int titanium) {
    m_currTitanium -= titanium;
}