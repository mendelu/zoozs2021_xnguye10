//
// Created by nuguy on 09.12.2021.
//

#include "PowerGenerator.h"

PowerGenerator::PowerGenerator(float maxFlyDistance, int shields) {
    m_lvl = 1;
    m_maxFlyDistance = maxFlyDistance;
    m_shields = shields;
    m_maxShields = shields;
}

int PowerGenerator::getMaxFlyDistance() {
    return m_maxFlyDistance;
}

int PowerGenerator::getShields() {
    return m_shields;
}

int PowerGenerator::getLvl() {
    return m_lvl;
}

void PowerGenerator::setShields(int dmg){
    if((m_shields - dmg) < 0){
        m_shields = 0;
    } else {
        m_shields = m_shields - dmg;
    }
}

void PowerGenerator::setShieldsToMax() {
    m_shields = m_maxShields;
}

void PowerGenerator::upgradePowerGenerator() {
    m_lvl++;
    m_shields += m_shields*0.2;
    m_maxFlyDistance++;
}
