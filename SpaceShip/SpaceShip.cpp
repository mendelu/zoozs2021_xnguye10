//
// Created by nuguy on 09.12.2021.
//

#include <iostream>
#include "SpaceShip.h"

SpaceShip::SpaceShip(std::string name, int coordinateX, int coordinateY, int hullHp) {
    m_name = name;
    m_coordinateX = coordinateX;
    m_coordinateY = coordinateY;
    m_hullHp = hullHp;
    m_maxHullHp = hullHp;
    m_lvl = 1;
    m_shipStorage = new ShipStorage(500, 500, 500);
    m_powerGenerator = new PowerGenerator(1, 200);
    m_weaponSystem.at(0) = new WeaponSystems("LaserCanon", 40);
    m_weaponSystem.at(1) = new WeaponSystems("RocketCanon", 60);
}

int SpaceShip::getShieldDmg(){
    return m_weaponSystem.at(0)->getDamagePerTurn();
}

int SpaceShip::getHullDmg(){
    return m_weaponSystem.at(1)->getDamagePerTurn();
}

int SpaceShip::getShieldHp(){
    return m_powerGenerator->getShields();
}

int SpaceShip::getHullHp(){
    return m_hullHp;
}

void SpaceShip::setHullHp(int dmg){
    if((m_hullHp - dmg) < 0){
        m_hullHp = 0;
    } else {
        m_hullHp = m_hullHp - dmg;
    }
}

void SpaceShip::setHullHpToMax() {
    m_hullHp = m_maxHullHp;
}

void SpaceShip::setShieldHp(int dmg){
    m_powerGenerator->setShields(dmg);
}

void SpaceShip::setShieldHpToMax() {
    m_powerGenerator->setShieldsToMax();
}

void SpaceShip::printInfo() {
    std::cout << "Name of a SpaceShip: " << m_name << std::endl;
    std::cout << "HP of a SpaceShip: " << m_hullHp << "HP" << std::endl;
    std::cout << "*Coordinates of a SpaceShip*" << std::endl;
    std::cout << "|" << m_coordinateX <<"x||" << m_coordinateY << "y" << "|" << std::endl;
    std::cout << "*Weapon systems*" << std::endl;
    std::cout << "|Name: " << m_weaponSystem.at(0)->getName() << "||Damage: " << m_weaponSystem.at(0)->getDamagePerTurn() << "|| Lvl: " << m_weaponSystem.at(0)->getLvl()<< "|" << std::endl;
    std::cout << "|Name: " << m_weaponSystem.at(1)->getName() << "||Damage: " << m_weaponSystem.at(1)->getDamagePerTurn() << "|| Lvl: " << m_weaponSystem.at(1)->getLvl() << "|" << std::endl;
    std::cout << "*Generator energy*" << std::endl;
    std::cout << "|Shields: " << m_powerGenerator->getShields() << "|| Maximum fly distance: " << m_powerGenerator->getMaxFlyDistance() << "||Lvl: " << m_powerGenerator->getLvl()<< "|" << std::endl;
    std::cout << "*Storage*" << std::endl;
    std::cout << "|Silicon: " << m_shipStorage->getCurrSilicon() << "| Max Silicon: " << m_shipStorage->m_maxSilicon << std::endl;
    std::cout << "|Wolfram: " << m_shipStorage->getCurrSilicon() << "|" << std::endl;
    std::cout << "|Titanium: " << m_shipStorage->getCurrTitanium() << "|" << std::endl;

}

void SpaceShip::printCombatInfo(){
    std::cout << " --- Combat information about SpaceShip --- " << std::endl;
    std::cout << "HP of a SpaceShip: " << m_hullHp << "HP" << std::endl;
    std::cout << "Shield of a SpaceShip: " << m_powerGenerator->getShields() << "HP" << std::endl;
    std::cout << "*Weapon systems*" << std::endl;
    std::cout << "|Name: " << m_weaponSystem.at(0)->getName() << "||Damage: " << m_weaponSystem.at(0)->getDamagePerTurn() << "|| Lvl: " << m_weaponSystem.at(0)->getLvl()<< "|" << std::endl;
    std::cout << "|Name: " << m_weaponSystem.at(1)->getName() << "||Damage: " << m_weaponSystem.at(1)->getDamagePerTurn() << "|| Lvl: " << m_weaponSystem.at(1)->getLvl() << "|" << std::endl;
    std::cout << std::endl;
}


void SpaceShip::moveToPlanet(int coordX, int coordY) {
    m_coordinateX = coordX;
    m_coordinateY = coordY;
}

int SpaceShip::getCoordinateX() {
    return m_coordinateX;
}

int SpaceShip::getCoordinateY() {
    return m_coordinateY;
}

int SpaceShip::getMaxFlyDistance() {
    return m_powerGenerator->getMaxFlyDistance();
}

void SpaceShip::addSilicon(int silicon) {
    m_shipStorage->addSilicon(silicon);
}

void SpaceShip::addWolfram(int wolfram) {
    m_shipStorage->addWolfram(wolfram);
}

void SpaceShip::addTitanium(int titanium) {
    m_shipStorage->addTitanium(titanium);
}

int SpaceShip::getEmptyStorageSilicon() {
    return m_shipStorage->m_maxSilicon - m_shipStorage->m_currSilicon;
}

int SpaceShip::getEmptyStorageWolfram() {
    return m_shipStorage->m_maxWolfram - m_shipStorage->m_currWolfram;
}

int SpaceShip::getEmptyStorageTitanium() {
    return m_shipStorage->m_maxTitanium - m_shipStorage->m_currTitanium + 0;
}

void SpaceShip::printHpAndShields(){
    std::cout << " --- SpaceShip --- " << std::endl;
    std::cout << " SpaceShip HP = " << m_hullHp << std::endl;
    std::cout << " SpaceShip Shields = " << m_powerGenerator->getShields() << std::endl;
}

int SpaceShip::getCurrSilicon() {
    return m_shipStorage->getCurrSilicon();
}

int SpaceShip::getCurrWolfram() {
    return m_shipStorage->getCurrWolfram();
}

int SpaceShip::getCurrTitanium() {
    return m_shipStorage->getCurrTitanium();
}

void SpaceShip::decreaseSilicon(int silicon) {
    m_shipStorage->decreaseSilicon( silicon);
}

void SpaceShip::decreaseWolfram(int wolfram) {
    m_shipStorage->decreaseSilicon( wolfram);
}

void SpaceShip::decreaseTitanium(int titanium) {
    m_shipStorage->decreaseSilicon( titanium);
}

void SpaceShip::upgradeSpaceShip() {
    std::string decision;
    int cost;
    cost = m_lvl*100;
            std::cout << "Upgrade will cost: " << cost << " Silicon, " << cost << " Wolfram, " << cost << " Titanium" << std::endl;
            std::cout << "You have:" << getCurrSilicon() << " Silicon, " << getCurrWolfram() << " Wolfram, " << getCurrTitanium() << " Titanium on SpaceShip." << std::endl;
            std:: cout << "Do you realy want to upgrade your spaceShip?  yes[1]/no[2]" << std::endl;
            std::cin >> decision;
            if (decision == "1"){
                if (getCurrSilicon() >= cost && getCurrWolfram() >= cost && getCurrTitanium() >= cost){
                    m_lvl++;
                    m_hullHp += m_hullHp*0.2;
                    m_shipStorage->decreaseSilicon(cost);
                    m_shipStorage->decreaseWolfram(cost);
                    m_shipStorage->decreaseTitanium(cost);
                    std::cout << "Your SpaceShip was upgraded." << std::endl;
                }else{
                    std::cout << "You don't have enough resources" << std::endl;
                }
            }
}

void SpaceShip::upgradeWeapon(int slot) {
    std::string decision;
    int cost;
    cost = m_weaponSystem[slot]->getLvl()*100;
    std::cout << "Upgrade will cost: " << cost << " Silicon, " << cost << " Wolfram, " << cost << " Titanium" << std::endl;
    std::cout << "You have:" << getCurrSilicon() << " Silicon, " << getCurrWolfram() << " Wolfram, " << getCurrTitanium() << " Titanium on SpaceShip." << std::endl;
    std:: cout << "Do you realy want to upgrade your " << m_weaponSystem[slot]->getName() << "?  yes[1]/no[2]" << std::endl;
    std::cin >> decision;
    if (decision == "1"){
        if (getCurrSilicon() >= cost && getCurrWolfram() >= cost && getCurrTitanium() >= cost){
            m_weaponSystem[slot]->upgradeWeapon(slot);
            m_shipStorage->decreaseSilicon(cost);
            m_shipStorage->decreaseWolfram(cost);
            m_shipStorage->decreaseTitanium(cost);
            std::cout << "Your " << m_weaponSystem[slot]->getName() << " was upgraded." << std::endl;
        }else{
            std::cout << "You don't have enough resources" << std::endl;
        }
    }
}

void SpaceShip::upgradePowerGenerator() {
    std::string decision;
    int cost;
    cost = m_powerGenerator->getLvl()*100;
    std::cout << "Upgrade will cost: " << cost << " Silicon, " << cost << " Wolfram, " << cost << " Titanium" << std::endl;
    std::cout << "You have:" << getCurrSilicon() << " Silicon, " << getCurrWolfram() << " Wolfram, " << getCurrTitanium() << " Titanium on SpaceShip." << std::endl;
    std:: cout << "Do you realy want to upgrade your PowerGenerator?  yes[1]/no[2]" << std::endl;
    std::cin >> decision;
    if (decision == "1"){
        if (getCurrSilicon() >= cost && getCurrWolfram() >= cost && getCurrTitanium() >= cost){
            m_powerGenerator->upgradePowerGenerator();
            m_shipStorage->decreaseSilicon(cost);
            m_shipStorage->decreaseWolfram(cost);
            m_shipStorage->decreaseTitanium(cost);
            std::cout << "Your PowerGenerator was upgraded." << std::endl;
        }else{
            std::cout << "You don't have enough resources" << std::endl;
        }
    }
}
