//
// Created by nuguy on 09.12.2021.
//

#ifndef OOPPROJECT_POWERGENERATOR_H
#define OOPPROJECT_POWERGENERATOR_H


class PowerGenerator {
    int m_lvl;
    int m_maxFlyDistance;
    int m_shields;
    int m_maxShields;
public:
    PowerGenerator(float maxFlyDistance, int shields);
    int getMaxFlyDistance();
    int getShields();
    int getLvl();
    void setShields(int dmg);
    void setShieldsToMax();
    void upgradePowerGenerator();
};


#endif //OOPPROJECT_POWERGENERATOR_H
