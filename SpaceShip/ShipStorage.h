//
// Created by nuguy on 09.12.2021.
//

#ifndef OOPPROJECT_SHIPSTORAGE_H
#define OOPPROJECT_SHIPSTORAGE_H


class ShipStorage {
public:
        int m_lvl;
        int m_maxSilicon;
        int m_maxWolfram;
        int m_maxTitanium;
        int m_currSilicon;
        int m_currWolfram;
        int m_currTitanium;
    public:
        ShipStorage(int maxSilicon, int maxWolfram, int maxTitanium);
        void lvlUp();
        int getLvl();
        int getCurrSilicon();
        int getCurrWolfram();
        int getCurrTitanium();
        int getMaxSilicon();
        int getMaxWolfram();
        int getMaxTitanium();
        int addSilicon(int silicon);
        int addWolfram(int wolfram);
        int addTitanium(int titanium);
        int decreaseSilicon(int silicon);
        int decreaseWolfram(int wolfram);
        int decreaseTitanium(int titanium);

};


#endif //OOPPROJECT_SHIPSTORAGE_H
