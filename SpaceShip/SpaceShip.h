//
// Created by nuguy on 09.12.2021.
//

#ifndef OOPPROJECT_SPACESHIP_H
#define OOPPROJECT_SPACESHIP_H


#include <string>
#include <array>
#include "WeaponSystems.h"
#include "PowerGenerator.h"
#include "ShipStorage.h"

class SpaceShip {
public:
    std:: string m_name;
    std::array<WeaponSystems*, 2> m_weaponSystem;
    int m_coordinateX;
    int m_coordinateY;
    PowerGenerator * m_powerGenerator;
    int m_hullHp;
    int m_maxHullHp;
    ShipStorage* m_shipStorage;
    int m_lvl;
    SpaceShip(std::string name, int coordinateX, int coordinateY, int hullHp);
    void printInfo();
    void printCombatInfo();
    void moveToPlanet(int coordX, int coordY);
    int getCoordinateX();
    int getCoordinateY();
    int getMaxFlyDistance();
    int getShieldDmg();
    int getHullDmg();
    int getShieldHp();
    int getHullHp();
    void setHullHp(int dmg);
    void setShieldHp(int dmg);
    void addSilicon(int silicon);
    void addWolfram(int wolfram);
    void addTitanium(int titanium);
    void decreaseSilicon(int silicon);
    void decreaseWolfram(int wolfram);
    void decreaseTitanium(int titanium);
    int getCurrSilicon();
    int getCurrWolfram();
    int getCurrTitanium();
    int getEmptyStorageSilicon();
    int getEmptyStorageWolfram();
    int getEmptyStorageTitanium();
    void printHpAndShields();
    void upgradeSpaceShip();
    void upgradeWeapon(int slot);
    void upgradePowerGenerator();
    void setHullHpToMax();
    void setShieldHpToMax();
};


#endif //OOPPROJECT_SPACESHIP_H
