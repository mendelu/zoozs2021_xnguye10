//
// Created by nuguy on 09.12.2021.
//

#ifndef OOPPROJECT_WEAPONSYSTEMS_H
#define OOPPROJECT_WEAPONSYSTEMS_H

#include <string>

class WeaponSystems {
    std:: string m_name;
    int m_damagePerTurn;
    int m_lvl;
public:
    WeaponSystems(std::string name, int damagePerTurn);
    int getDamagePerTurn();
    std::string getName();
    int getLvl();
    void upgradeWeapon(int slot);
};


#endif //OOPPROJECT_WEAPONSYSTEMS_H
