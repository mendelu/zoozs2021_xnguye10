//
// Created by mispi on 09.12.2021.
//

#ifndef OOPPROJECT_GAMEENGINE_H
#define OOPPROJECT_GAMEENGINE_H

#include <iostream>
#include <algorithm>
#include "World/Map.h"
#include "Planet/Planet.h"
#include "SpaceShip/SpaceShip.h"
#include "OptionsValidator.h"

class GameEngine {
    Map* m_map;
    SpaceShip* m_spaceship;
    std::vector<Planet*> m_occupiedPlanets;
    Planet* m_onPlanet;
    int m_round;
public:
    GameEngine();
    void playGame();
    void worldInicialization();
    void shipInicialization();
    void decisionOptions();
    void managePlanets();
    void planetMenu();
    void buildingMenu();
    void planetsInRange();
    void choosePlanetToTravel();
    void showInfo();
    void showOccupiedPlanets();
    void showMaterialsOnOccupiedPlanets();
    void endRound();
    void attackPlanet(Planet* planet);
    Map* getMap();
    SpaceShip* getSpaceship();
    bool isNumber(const std::string& str);
    void spaceShipMenu();
    void transferSilicon();
    void transferWolfram();
    void transferTitanium();
    void upgradeSpaceShip();
    void upgradeWeaponSystem();
    void upgradePowerGenerator();
};


#endif //OOPPROJECT_GAMEENGINE_H
