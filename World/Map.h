//
// Created by mispi on 09.12.2021.
//

#ifndef ZOOZS2021_XNGUYE10_MAP_H
#define ZOOZS2021_XNGUYE10_MAP_H

#include <iostream>
#include <iomanip>
#include <vector>
#include "../Planet/Planet.h"
#include "../SpaceShip/SpaceShip.h"

struct Size{
    int x;
    int y;
};

class Map {
private:
    std::vector<std::vector<Planet*>> m_map;
    Size m_size;
    unsigned int m_numOfRows;
    unsigned int m_numOfCols;
public:
    Map(Size size);
    //Location* getLocation(const unsigned int row, const unsigned int col);
    void insertLocation(const unsigned int row, const unsigned int col, Planet* planet);
    Planet* getPlanetFromMap(const unsigned int row, const unsigned int col);
    void printMap(int shipCoordX, int shipCoordY);
    int getNumberOfRows();
    int getNumberOfCols();
    bool emptyLocation(int row, int col);
    std::string getNameOfPlanet(int row, int col);
    int getXcordOfPlanet(std::string name);
    int getYcordOfPlanet(std::string name);
};

#endif //ZOOZS2021_XNGUYE10_MAP_H
