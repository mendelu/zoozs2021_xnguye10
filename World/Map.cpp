//
// Created by mispi on 09.12.2021.
//

#include "Map.h"

Map::Map(Size size){

    std::vector<Planet*> helpRow(size.x, 0);
    m_map.resize(size.x);
    for(int i=0; i < size.x; i++){
        m_map[i] = helpRow;
    }
    m_numOfCols = size.y;
    m_numOfRows = size.x;
}

void Map::insertLocation(const unsigned int row, const unsigned int col, Planet* planet) {
    if((row < m_numOfRows) and (col < m_numOfCols)){
        m_map[row][col] = planet;
    } else {
        std::cerr << "Map::getLocation - Rozsah mimo desky" << std::endl;
        std::cerr << "Pozadadovana pozice [" << row << "," << col << "]." << std::endl;
        std::cerr << "Rozsah desky je [" << m_numOfRows << "," << m_numOfCols << "]." << std::endl;
    }
}

void Map::printMap(int shipCoordX, int shipCoordY){

    std::cout << "-------------------------------------Map------------------------------------" << std::endl;
    for(int row = 0; row < m_numOfRows; row++) {
        for (int col = 0; col < m_numOfCols; col++) {
            int cislo = 0;
            int delka = 0;
            int zbyva = 0;
            if(m_map[row][col] != nullptr and shipCoordX == row and shipCoordY == col) {
                delka = m_map[row][col]->getName().size()+1;
                cislo = ((15-delka)/2);
                zbyva = 15-cislo-delka;
                std::cout << "|" << std::setw(cislo) << "" <<  "*" << m_map[row][col]->getName() << std::setw(zbyva);
            } else if(m_map[row][col] != nullptr){
                delka = m_map[row][col]->getName().size();
                cislo = ((15-delka)/2)+delka;
                std::cout << "|" << std::setw(cislo) << m_map[row][col]->getName() << std::setw(15-cislo);
            } else if (shipCoordX == row and shipCoordY == col) {
                cislo = ((15-1)/2);
                std::cout << "|" << std::setw(cislo) << "*" << std::setw(15-cislo);
            }else {
                std::cout << "|" << std::setw(15);
            }
            if(col == m_numOfCols-1){
                std::cout << "|";
            }
        }
        std::cout << std::endl;
        std::cout << std::setw(75) << "----------------------------------------------------------------------------" << std::endl;
    }
}

int Map::getNumberOfRows() {
    return m_numOfRows;
}

int Map::getNumberOfCols() {
    return m_numOfCols;
}

std::string Map::getNameOfPlanet(int row, int col) {
    return m_map[row][col]->getName();
}

bool Map::emptyLocation(int row, int col) {
    if(m_map[row][col] != nullptr) {
        return true;
    } else {
        return false;
    }

}

int Map::getXcordOfPlanet(std::string name) {
    int codX = -1;
    for(int row = 0; row < m_numOfRows; row++) {
        for (int col = 0; col < m_numOfCols; col++) {
            if(m_map[row][col] != nullptr and m_map[row][col]->getName() == name) {
                codX = row;
            }
        }
    }
    return codX;
}

int Map::getYcordOfPlanet(std::string name) {
    int codY = -1;
    for(int row = 0; row < m_numOfRows; row++) {
        for (int col = 0; col < m_numOfCols; col++) {
            if(m_map[row][col] != nullptr and getNameOfPlanet(row, col) == name) {
                codY = col;
            }
        }
    }
    return codY;
}

Planet*::Map::getPlanetFromMap(const unsigned int row, const unsigned int col) {
    if(m_map[row][col] != nullptr){
        return m_map[row][col];
    } else {
        return 0;
    }
}