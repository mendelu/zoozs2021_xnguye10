//
// Created by mispi on 09.12.2021.
//

#include "GameEngine.h"

GameEngine::GameEngine() {
    m_map = nullptr;
    m_round = 1;
    m_onPlanet = nullptr;
}

void GameEngine::worldInicialization(){

    Planet* planet1 = new Planet("Mars",50,100,100,100);
    Planet* planet2 = new Planet("Jupiter",50,150,150,150);
    Planet* planet3 = new Planet("Saturn",200,300,300,300);
    Planet* planet4 = new Planet("Uran",250,400,500,500);
    Planet* planet5 = new Planet("Terra",10,10,100,100);
    Planet* planet6 = new Planet("Solaria",200,250,250,300);
    Planet* planet7 = new Planet("Gezerth",150,200,200,250);
    Planet* planet8 = new Planet("Terminus",300,300,500,400);
    Planet* planet9 = new Planet("Argo",200,50,150,200);
    Planet* planet10 = new Planet("Astarte",350,350,500,500);

    Size newSize{7,5};

    m_map = new Map(newSize);
    m_map->insertLocation(3,1,planet1);
    m_map->insertLocation(1,2,planet2);
    m_map->insertLocation(3,4,planet3);
    m_map->insertLocation(2,0,planet4);
    m_map->insertLocation(3,2,planet5);
    m_map->insertLocation(1,4,planet6);
    m_map->insertLocation(1,0,planet7);
    m_map->insertLocation(5,4,planet8);
    m_map->insertLocation(6,2,planet9);
    m_map->insertLocation(6,0,planet10);
}

void GameEngine::shipInicialization(){
    std::string name;
    std::cout << "Choose a name for your ship:" << std::endl;
    std::cin>>name;
    m_spaceship = new SpaceShip(name, 3, 2, 500);
    m_occupiedPlanets.push_back(m_map->getPlanetFromMap(3,2));
    m_map->getPlanetFromMap(3,2)->setConqueredToTrue();
    m_onPlanet = m_map->getPlanetFromMap(m_map->getXcordOfPlanet("Terra"),
                                         m_map->getYcordOfPlanet("Terra"));

    m_spaceship->m_shipStorage->addSilicon(200);
    m_spaceship->m_shipStorage->addWolfram(200);
    m_spaceship->m_shipStorage->addTitanium(200);
}

void GameEngine::decisionOptions() {
    /*//Přidání planety
    m_occupiedPlanets.push_back(m_map->getPlanetFromMap(0,1));
    m_map->getPlanetFromMap(0,1)->setConqueredToTrue();*/

    std::string inputDecision;
    int decision;
    bool menu = true;
    std::string continueHandler;
    bool end = false;
    std::string attack;
    while (decision != 100 and end == false){
        if(menu == true){
        std::cout << "What do you want to do? - Round " << m_round << "." << std::endl;
        std::cout << "[1] - End round" << std::endl;
        std::cout << "[2] - Show map" << std::endl;
        std::cout << "[3] - Information about my SpaceShip" << std::endl;
        std::cout << "[4] - Travel to planet" << std::endl;
        if(!m_occupiedPlanets.empty()){
                std::cout << "[5] - Manage planets" << std::endl;
            }
        if(m_onPlanet != nullptr && !(m_onPlanet->getConquered())){
            std::cout << "[6] - Attack planet" << std::endl;
        }
        if(m_onPlanet != nullptr && m_onPlanet->getConquered()){
                std::cout << "[6] - Planet menu" << std::endl;
                std::cout << "[7] - Manage SpaceShip" << std::endl;
        }
        std::cout << "[111] - Hint" << std::endl;
        std::cout << "[100] - End the game" << std::endl;
        std::cin >> inputDecision;
            if (isNumber(inputDecision) != true) {
                std::cout << "Not an option." << std::endl;
                decisionOptions();
            }else{
                decision = std::stoi(inputDecision);
            }
        menu = false;
        }
            switch (decision) {
                case 1:
                    endRound();
                    break;
                case 2:
                    m_map->printMap(m_spaceship->getCoordinateX(), m_spaceship->getCoordinateY());
                    std::cout << std::endl;
                    break;
                case 3:
                    m_spaceship->printInfo();
                    std::cout << std::endl;
                    break;
                case 4:
                    planetsInRange();
                    choosePlanetToTravel();
                    break;
                case 5:
                    managePlanets();
                    break;
                case 6:
                    if(m_onPlanet != nullptr && !(m_onPlanet->getConquered())){
                        showInfo();
                    }else if(m_onPlanet != nullptr && m_onPlanet->getConquered()){
                        planetMenu();
                    }
                    break;
                case 7:
                    if(m_onPlanet != nullptr && m_onPlanet->getConquered()) {
                        spaceShipMenu();
                    }
                    break;
                case 111:
                    std::cout << "Your SpaceShip is displayed on the map as *:" << std::endl;
                    std::cout << std::endl;
                    break;
            default:
                std::cout << "Not a valid answer" << std::endl;
                decisionOptions();
            }
        decision = 0;
        std::cout << "Write anything to continue" << std::endl;
        std::cout << "End the game [100]" << std::endl;
        std::cin >> continueHandler;
        if (continueHandler == "100"){
            end = true;
        }else {
            menu = true;
        }
    }
}

void GameEngine::managePlanets() {
    std::string decision;
        std::cout << "[1] - Occupied planets" << std::endl;
        std::cout << "[2] - Materials on my planets" << std::endl;
        std::cout << "[3] - Back" << std::endl;

    std::cin>>decision;
    if (isNumber(decision) != true) {
        std::cout << "Not an option." << std::endl;
        managePlanets();
    }
    switch(std::stoi(decision)) {
        case 1:
            showOccupiedPlanets();
            break;
        case 2:
            showMaterialsOnOccupiedPlanets();
            break;
        case 3:
            decisionOptions();
        default:
            std::cout << "Not a valid answer" << std::endl;
            managePlanets();
    }
    decisionOptions();
}

void GameEngine::planetMenu() {
    std::string decision;
    std::cout << "[1] - Manage buildings" << std::endl;
    std::cout << "[2] - Planet info" << std::endl;
    std::cout << "[3] - Production info" << std::endl;
    std::cout << "[4] - Scan for additional resource" << std::endl;
    std::cout << "[5] - Back" << std::endl;
    std::cin >> decision;
    if (isNumber(decision) != true) {
        std::cout << "Not an option." << std::endl;
        planetMenu();
    }
    switch(std::stoi(decision)) {
        case 1:
            buildingMenu();
            break;
        case 2:
            m_onPlanet->printInfoBasic();
            break;
        case 3:
            m_onPlanet->printInfoProduction();
            break;
        case 4:
            m_onPlanet->resourceSearch();
            break;
        case 5:
            decisionOptions();
            break;
        default:
            std::cout << "Not a valid answer" << std::endl;
            planetMenu();
    }
    planetMenu();
}

void GameEngine::buildingMenu() {
    std::string decision;
    int slot;
    m_onPlanet->printInfoBuildings();
    std::cout << "[1] - New building" << std::endl;
    std::cout << "[2] - Upgrade building" << std::endl;
    std::cout << "[3] - Back" << std::endl;
    std::cin >> decision;
    if (isNumber(decision) != true) {
        std::cout << "Not an option." << std::endl;
        buildingMenu();
    }
    switch(std::stoi(decision)) {
        case 1:
            m_onPlanet->build();
            break;
        case 2:
            std::cout << "Choose slot of building you want to upgrade:" << std::endl;
            std::cin >> slot;
            if (slot<=10 && slot>=1){
                m_onPlanet->upgradeBuilding(slot);
            }else{
                std::cout << "Choose a valid slot." << std::endl;
            }
            break;
        case 3:
            planetMenu();
            break;
        default:
            std::cout << "Not a valid answer" << std::endl;
            buildingMenu();
    }
    buildingMenu();
}

void GameEngine::playGame(){
    worldInicialization();
    shipInicialization();
    decisionOptions();
}

void GameEngine::planetsInRange(){
    std::cout << "Planets in range: " << std::endl;
    m_spaceship->getCoordinateX() + m_spaceship->getCoordinateY();

    for (int row = 0; row < m_map->getNumberOfRows(); row++) {
        for (int col = 0; col < m_map->getNumberOfCols(); col++) {
            if (m_map->emptyLocation(row, col)) {
                //if( 0 < ((row+col) - ((m_spaceship->getCoordinateX() + m_spaceship->getCoordinateY())))){
                if (m_spaceship->getMaxFlyDistance() >= abs(((row + col) -
                                                             (m_spaceship->getCoordinateX() +
                                                              m_spaceship->getCoordinateY())))) {
                    std::cout << m_map->getNameOfPlanet(row, col) << std::endl;
                }
                //}
            }
        }
    }

    std::cout << std::endl;

}

void GameEngine::choosePlanetToTravel(){
    std::cout << "Write the name of the planet where you want to travel." << std::endl;
    std::string newPlanet;
    std::cin >> newPlanet;
    if (m_map->getYcordOfPlanet(newPlanet) >= 0 and (m_spaceship->getMaxFlyDistance() >=
                                                     abs(((m_spaceship->getCoordinateX() +
                                                           m_spaceship->getCoordinateY()) -
                                                          (m_map->getXcordOfPlanet(newPlanet) +
                                                           m_map->getYcordOfPlanet(newPlanet)))))) {
        m_spaceship->moveToPlanet(m_map->getXcordOfPlanet(newPlanet),
                                  m_map->getYcordOfPlanet(newPlanet));
        std::cout << "You moved to the planet: " << newPlanet << std::endl;
        m_onPlanet = m_map->getPlanetFromMap(m_map->getXcordOfPlanet(newPlanet),
                                             m_map->getYcordOfPlanet(newPlanet));
        if(m_onPlanet->getConquered()){
            m_spaceship->setShieldHpToMax();
            m_spaceship->setHullHpToMax();
        }
    } else {
        std::cout << "Planet with this name does not exists." << std::endl;
    }
    std::cout << std::endl;
}

void GameEngine::showInfo(){
    std::string response;
    m_spaceship->printCombatInfo();
    m_map->getPlanetFromMap(m_map->getXcordOfPlanet(m_onPlanet->getName()), m_map->getYcordOfPlanet(m_onPlanet->getName()))->printInfoCombat();
    std::cout << "Do you really want to attack planet? It will end your round. - [1]Yes/[*]No" << std::endl;
    std::cin >> response;
    if(response == "1"){
        attackPlanet(m_map->getPlanetFromMap(m_map->getXcordOfPlanet(m_onPlanet->getName()), m_map->getYcordOfPlanet(m_onPlanet->getName())));
    } else {
        std::cout << std::endl;
    }
}

void GameEngine::attackPlanet(Planet* planet){
    //HP AND DMG INFORMATION ABOUT PLANET
    int planetHullDmg = planet->getHullDamage();
    int planetShieldDmg = planet->getShieldDamage();
    int planetHp = planet->getPlanetDefenceHp();
    int planetShieldHp = planet->getPlanetShieldHp();
    //HP AND DMG INFORMATION ABOUT SPACESHIP
    int shipShieldHp = m_spaceship->getShieldHp();
    int shipHullHp = m_spaceship->getHullHp();
    int shipShieldDmg = m_spaceship->getShieldDmg();
    int shipHullDmg = m_spaceship->getHullDmg();

    while((planetHp > 0 || planetShieldHp > 0) || (shipShieldHp > 0 || shipHullHp > 0)){

        std::string response;
        std::cout << std::endl;

        //SHIP ATTACKING PLANET
        if(planetShieldHp > 0){
            planet->setPlanetShieldHp(shipShieldDmg);
        } else if(planetHp > 0){
            planet->setPlanetDefenceHp(shipHullDmg);
        }

        //CHECK IF PLANET HAS 0 HP
        if(planet->getPlanetShieldHp() == 0 && planet->getPlanetDefenceHp() == 0){
            std::cout << "Planet has been conquered" << std::endl;
            planet->setConqueredToTrue();
            m_occupiedPlanets.push_back(planet);
            m_spaceship->setShieldHpToMax();
            m_spaceship->setHullHpToMax();
            planet->setPlanetDefenceHpToMax();
            planet->setPlanetShieldHpToMax();
            break;
        }


        //PLANET ATTACKING SHIP
        if(planet->getPlanetShieldHp() > 0 || planet->getPlanetDefenceHp() > 0){
            if(shipShieldHp > 0){
                m_spaceship->setShieldHp(planetShieldDmg);
            } else if(shipHullHp > 0) {
                m_spaceship->setHullHp(planetHullDmg);
            }
        }

        if(m_spaceship->getShieldHp() == 0 && m_spaceship->getHullHp() == 0){
            std::string decision;
            std::cout << " --- Your SpaceShip was destroyed --- " << std::endl;
            std::cout << "Do you want to restart? [1]Yes/[*]No" <<std::endl;
            if(decision == "1"){
                shipInicialization();
                worldInicialization(); decisionOptions();
            }else{
                exit(3);
            }
        }

        planetHp = planet->getPlanetDefenceHp();
        planetShieldHp = planet->getPlanetShieldHp();
        shipShieldHp = m_spaceship->getShieldHp();
        shipHullHp = m_spaceship->getHullHp();

        m_spaceship->printHpAndShields();
        planet->printHpAndShields();

        endRound();

        std::cout << "Do you want to keep fighting? [*]Yes/[0]No" << std::endl;
        std::cin >> response;
        if(response == "0"){
            planet->setPlanetShieldHpToMax();
            planet->setPlanetDefenceHpToMax();
            break;
        }
    }
}

void GameEngine::showOccupiedPlanets(){
    for(auto planet : m_occupiedPlanets){
        std::cout << planet->getName() << std::endl;
    }
}

void GameEngine::showMaterialsOnOccupiedPlanets(){
    for(auto planet : m_occupiedPlanets){
        std::cout << " ----- " << planet->getName() << " ----- " << std::endl;
        std::cout << "Public order" << planet->getPublicOrder() << "(" << planet->getPublicOrderChangePerTurn() << ")" << std::endl;
        planet->printInfoProduction();
        std::cout << std::endl;
    }
}

void GameEngine::endRound(){
    m_round++;
    std::cout << std::endl << " --- The end of the round --- " << std::endl << std::endl;
    if(!m_occupiedPlanets.empty()){
        for(auto planet : m_occupiedPlanets){
            planet->updatePublicOrder();
            planet->updateStorage();
        }
        for(auto planet : m_occupiedPlanets){
            if(!planet->getConquered()){
                m_occupiedPlanets.erase(std::remove(m_occupiedPlanets.begin(), m_occupiedPlanets.end(), planet), m_occupiedPlanets.end());
                std::cout << planet->getName() << std::endl;
            }
        }
    }
}

Map *GameEngine::getMap() {
    return m_map;
}

SpaceShip *GameEngine::getSpaceship() {
    return m_spaceship;
}

bool GameEngine::isNumber(const std::string& str)
{
    for (char const &c : str) {
        if (std::isdigit(c) == 0) return false;
    }
    return true;
}

void GameEngine::spaceShipMenu(){
    std::string decision;
    std::string resource;
    std::cout << "[1] - Transfer Silicon" << std::endl;
    std::cout << "[2] - Transfer Wolfram" << std::endl;
    std::cout << "[3] - Transfer Titanium" << std::endl;
    std::cout << "[4] - Upgrade SpaceShip" << std::endl;
    std::cout << "[5] - Upgrade WeaponSystems" << std::endl;
    std::cout << "[6] - Upgrade PowerGenerator" << std::endl;
    std::cout << "[7] - Back" << std::endl;
    std::cin >> decision;
    if (isNumber(decision) != true) {
        std::cout << "Not an option." << std::endl;
        spaceShipMenu();
    }
    switch(std::stoi(decision)) {
        case 1:
            transferSilicon();
            break;
        case 2:
            transferWolfram();
            break;
        case 3:
            transferTitanium();
            break;
        case 4:
            upgradeSpaceShip();
            break;
        case 5:
            upgradeWeaponSystem();
            break;
        case 6:
            upgradePowerGenerator();
            break;
        case 7:
            decisionOptions();
            break;
        default:
            std::cout << "Not a valid answer" << std::endl;
            spaceShipMenu();
    }
    spaceShipMenu();
}

void GameEngine::transferSilicon() {
    std::string response;
    std::string resource;
    std::cout << "In SpaceShip storage is: " << m_spaceship->getCurrSilicon() << " Silicon" << std::endl;
    std::cout << "In planet storage is: " << m_onPlanet->getSilicon() << " Silicon" << std::endl;
    std::cout << "[1] - Load" << std::endl;
    std::cout << "[2] - Unload" << std::endl;
    std::cout << "[3] - Back" << std::endl;
    std::cin>>response;
    if (isNumber(response) != true) {
        std::cout << "Not an option." << std::endl;
        transferSilicon();
    }
    switch(std::stoi(response)) {
        case 1:
            std::cout << "How much do you want to transfer?" << std::endl;
            std::cin >> resource;
            if (isNumber(resource)){
                if(m_spaceship->getEmptyStorageSilicon() >=  std::stoi(resource) && m_onPlanet->getSilicon() >= std::stoi(resource)){
                    m_spaceship->addSilicon(std::stoi(resource));
                    m_onPlanet->decreaseSilicon(std::stoi(resource));
                }else{
                    std::cout << "Wrong amount!" << std::endl;
                }
            }else{
                std::cout << "That was not a number!" << std::endl;
            }
            break;
        case 2:
            std::cout << "How much do you want to transfer?" << std::endl;
            std::cin >> resource;
            if (isNumber(resource)){
                if(m_onPlanet->getEmptyStorageSilicon() >=  std::stoi(resource) && m_spaceship->getCurrSilicon() >= std::stoi(resource)){
                    m_spaceship->decreaseSilicon(std::stoi(resource));
                    m_onPlanet->addSilicon(std::stoi(resource));
                }else{
                    std::cout << "Wrong amount!" << std::endl;
                }
            }else{
                std::cout << "That was not a number!" << std::endl;
            }
            break;
        case 3:
            spaceShipMenu();
            break;
        default:
            std::cout << "Not a valid answer" << std::endl;
            spaceShipMenu();
    }
    spaceShipMenu();
}

void GameEngine::transferWolfram() {
    std::string response;
    std::string resource;
    std::cout << "In SpaceShip storage is: " << m_spaceship->getCurrWolfram() << " Wolfram" << std::endl;
    std::cout << "In planet storage is: " << m_onPlanet->getWolfram() << " Wolfram" << std::endl;
    std::cout << "[1] - Load" << std::endl;
    std::cout << "[2] - Unload" << std::endl;
    std::cout << "[3] - Back" << std::endl;
    std::cin>>response;
    if (isNumber(response) != true) {
        std::cout << "Not an option." << std::endl;
        transferWolfram();
    }
    switch(std::stoi(response)) {
        case 1:
            std::cout << "How much do you want to transfer?" << std::endl;
            std::cin >> resource;
            if (isNumber(resource)){
                if(m_spaceship->getEmptyStorageWolfram() >=  std::stoi(resource) && m_onPlanet->getWolfram() >= std::stoi(resource)){
                    m_spaceship->addWolfram(std::stoi(resource));
                    m_onPlanet->decreaseWolfram(std::stoi(resource));
                }else{
                    std::cout << "Wrong amount!" << std::endl;
                }
            }else{
                std::cout << "That was not a number!" << std::endl;
            }
            break;
        case 2:
            std::cout << "How much do you want to transfer?" << std::endl;
            std::cin >> resource;
            if (isNumber(resource)){
                if(m_onPlanet->getEmptyStorageWolfram() >=  std::stoi(resource) && m_spaceship->getCurrWolfram() >= std::stoi(resource)){
                    m_spaceship->decreaseWolfram(std::stoi(resource));
                    m_onPlanet->addWolfram(std::stoi(resource));
                }else{
                    std::cout << "Wrong amount!" << std::endl;
                }
            }else{
                std::cout << "That was not a number!" << std::endl;
            }
            break;
        case 3:
            spaceShipMenu();
            break;
        default:
            std::cout << "Not a valid answer" << std::endl;
            spaceShipMenu();
    }
    spaceShipMenu();
}

void GameEngine::transferTitanium() {
    std::string response;
    std::string resource;
    std::cout << "In SpaceShip storage is: " << m_spaceship->getCurrTitanium() << " Titanium" << std::endl;
    std::cout << "In planet storage is: " << m_onPlanet->getTitanium() << " Titanium" << std::endl;
    std::cout << "[1] - Load" << std::endl;
    std::cout << "[2] - Unload" << std::endl;
    std::cout << "[3] - Back" << std::endl;
    std::cin>>response;
    if (isNumber(response) != true) {
        std::cout << "Not an option." << std::endl;
        transferTitanium();
    }
    switch(std::stoi(response)) {
        case 1:
            std::cout << "How much do you want to transfer?" << std::endl;
            std::cin >> resource;
            if (isNumber(resource)){
                if(m_spaceship->getEmptyStorageTitanium() >=  std::stoi(resource) && m_onPlanet->getTitanium() >= std::stoi(resource)){
                    m_spaceship->addTitanium(std::stoi(resource));
                    m_onPlanet->decreaseTitanium(std::stoi(resource));
                }else{
                    std::cout << "Wrong amount!" << std::endl;
                }
            }else{
                std::cout << "That was not a number!" << std::endl;
            }
            break;
        case 2:
            std::cout << "How much do you want to transfer?" << std::endl;
            std::cin >> resource;
            if (isNumber(resource)){
                if(m_onPlanet->getEmptyStorageTitanium() >=  std::stoi(resource) && m_spaceship->getCurrTitanium() >= std::stoi(resource)){
                    m_spaceship->decreaseTitanium(std::stoi(resource));
                    m_onPlanet->addTitanium(std::stoi(resource));
                }else{
                    std::cout << "Wrong amount!" << std::endl;
                }
            }else{
                std::cout << "That was not a number!" << std::endl;
            }
            break;
        case 3:
            spaceShipMenu();
            break;
        default:
            spaceShipMenu();
    }
    spaceShipMenu();
}

void GameEngine::upgradeSpaceShip() {
    if(m_spaceship->m_lvl >= 3){
        std::cout << "Your spaceShip is on max lvl: " << m_spaceship->m_lvl << std::endl;
    }else{
        std::cout << "Your SpaceShip is lvl: " << m_spaceship->m_lvl << std::endl;
        m_spaceship->upgradeSpaceShip();
    }
}

void GameEngine::upgradeWeaponSystem() {
    std::string decision;

    std::cout << "[1] - Upgrade LaserCanon" << std::endl;
    std::cout << "[2] - Upgrade RocketCanon" << std::endl;
    std::cout << "[3] - Back" << std::endl;
    std::cin >> decision;
    if (isNumber(decision)){
        switch (std::stoi(decision)) {
            case 1:
                if(m_spaceship->m_weaponSystem[0]->getLvl() <= 3) {
                    m_spaceship->upgradeWeapon(0);
                }else{
                    std::cout << "Your LaserCanon is on max lvl: " << m_spaceship->m_weaponSystem[0]->getLvl() << std::endl;
                }
            break;
            case 2:
                if(m_spaceship->m_weaponSystem[1]->getLvl() <= 3) {
                    m_spaceship->upgradeWeapon(1);
                }else{
                    std::cout << "Your LaserCanon is on max lvl: " << m_spaceship->m_weaponSystem[1]->getLvl() << std::endl;
                }
            break;
            case 3:
                spaceShipMenu();
                break;
            default:
                std::cout << "Not a valid answer" << std::endl;
                upgradeWeaponSystem();
        }
    }else{
        std::cout << "That was not a number!" << std::endl;
    }
}

void GameEngine::upgradePowerGenerator() {
    if(m_spaceship->m_powerGenerator->getLvl() <= 3) {
        m_spaceship->upgradePowerGenerator();
    }else{
        std::cout << "Your PowerGenerator is on max lvl: " << m_spaceship->m_powerGenerator->getLvl() << std::endl;
    }
}


